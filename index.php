<?php

require_once("class/Path.php");

echo "<hr />Raw array of paths:<hr />";
Path::printPath();
// First Method
echo "<hr />First Method Result:<hr />";
Path::convertPathArrToDirArr();
// Second Method
echo "<hr />Second Method Result:<hr />";
Path::printDir(5, 5);
// Third Method
echo "<hr />Third Method Result:<hr />";
echo "<pre>", print_r(Path::generateFilePath('/home/user/', 5, 6, 2), true), "</pre>";