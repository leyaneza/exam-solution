<?php

class Path
{
    private static $path = [
        '/home/user/folder1/folder2/kdh4kdk8.txt',
        '/home/user/folder1/folder2/565shdhh.txt',
        '/home/user/folder1/folder2/folder3/nhskkuu4.txt',
        '/home/user/folder1/iiskjksd.txt',
        '/home/user/folder1/folder2/folder3/owjekksu.txt',
    ];

    public static $convertedArray = [];

    private static $crawl = 0;

    public static function getConvertedArray()
    {
        return self::$convertedArray;
    }
    /**
     * Print Path
     *
     * @return void
     */
    public static function printPath()
    {
        echo "<pre>", print_r(self::$path, true), "</pre>";
    }

    /**
     * Convert to Array Dir
     *
     * @return void
     */
    public static function convertPathArrToDirArr(): void
    {
        $dir = [];
        // Ascending Sort
        usort(self::$path, function ($a, $b) {
            return  count(explode('/', $a)) - count(explode('/', $b));
        });
        // Loop thru path
        foreach(self::$path as $itm) {
            $x = 0;
            $pathArr = explode('/', preg_replace('/\//', '', $itm, 1));
            $ctr = count($pathArr);
            self::_recursiveCheck($pathArr, $pathArr[0], $dir, $x);
        }
        // Print
        echo "<pre>", print_r($dir, true), "</pre>";
        // Assign to property
        self::$convertedArray = $dir;
    }

    /**
     * Parse Array Directory
     *
     * @param array $pathArr
     * @param [type] $val
     * @param array $dir
     * @param int $x
     * @return void
     */
    private static function _recursiveCheck(
        array $pathArr,
        $val,
        array &$dir,
        int $x
    ): void {
        // If Array key does not exist inside the current array recursion
        if(!array_key_exists($val, $dir)) {
            // If last occurence has .
            if(strripos($val, '.') != false) {
                $dir[] = $val;
            } else {
                $dir[$val] = [];
            }
        }
        // Increment x counter
        $x++;
        // If x is less than total count of path
        if($x < count($pathArr)) {
            // Recursion
            self::_recursiveCheck($pathArr, $pathArr[$x], $dir[$val], $x);
        }

        return;
    } 

    /**
     * Print Directory
     *
     * @param integer $depth
     * @param integer $leaf
     * @return void
     */
    public static function printDir(
        int $depth,
        int $leaf
    ): void {
        $lf = 0;
        $tree = &self::$convertedArray;
        $crawl = &self::$crawl;
        // Check depth and crawl level
        if($depth < 1 && $crawl == 0) {
            return;
        }
        // Loop tree
        foreach($tree as $i => $item) {
            if($lf < $leaf) {
                // 4 * depth Spacing
                echo str_repeat('&nbsp;', (4 * $crawl));
                // Print Depth
                // if current depth !string
                if(!is_string($item)) {
                    echo $i . "<br/>";
                } else {
                    echo $item . "<br/>";
                }
                // Set next depth
                $tree = (is_array($item)) ? $item : [];
            }
            $lf++;
        }
        // Decrement $depth & Increment $crawl
        $depth--;
        $crawl++;
        // Check depth and crawl level
        if($depth >= 0 && $crawl > 0) {
            // Recursion
            self::printDir($depth, $leaf);
        }

        return;
    }

    /**
     * Generate File Paths
     *
     * @param string $basePath
     * @param integer $pathsReturned
     * @param integer $pathDepth
     * @param integer $fileCount
     * @return array
     */
    public static function generateFilePath(
        string $basePath = '',
        int $pathsReturned = 0,
        int $pathDepth = 0,
        int $fileCount = 0
    ) : array {

        $pathArray = [];
        $pathTemplate = $basePath;
        $charLimit = 8;
        // Generate File Paths while $pathReturned > 0
        while($pathsReturned > 0) {
            $path = $pathTemplate;
            $depth = rand(1, $pathDepth);
            $fileQty = rand(1, $fileCount);
            // Generate Depth
            for($i = 0; $i < $depth; $i++) {
                $path .= 'folder' . ($i+1) . '/';
            }
            // Generate File Name
            while($fileQty > 0) {
                if($pathsReturned > 0) {
                    $pathArray[] = $path . self::generateFileName($charLimit);
                    $pathsReturned--;
                }
                // Decrement $fileQty
                $fileQty--;
            }
        }

        return $pathArray;
    }

    /**
     * Generate Random Filename
     *
     * @param integer $charLimit
     * @return void
     */
    private static function generateFileName(int $charLimit = 0) {
        $charHaystack = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($charHaystack);
        $fileName = '';
        // Loop and generate random characters
        for ($i = 0; $i < $charLimit; $i++) {
            $fileName .= $charHaystack[rand(0, $charactersLength - 1)];
        }

        return $fileName . '.txt';
    }
}